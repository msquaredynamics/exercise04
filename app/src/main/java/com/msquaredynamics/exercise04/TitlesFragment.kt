package com.msquaredynamics.exercise04

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import android.widget.TextView
import com.msquaredynamics.exercise04.utils.BasicItemTouchHelperCallback
import kotlinx.android.synthetic.main.fragment_titles.*
import kotlinx.android.synthetic.main.listitem_fragment_titles.view.*


class TitlesFragment : Fragment() {

    private var listener: OnArticleSelectedListener? = null // Listener for passing click events to the activity

    private lateinit var viewLayoutManager: LinearLayoutManager
    private lateinit var viewAdapter: TitlesListAdapter
    private lateinit var itemTouchHelper: ItemTouchHelper



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewLayoutManager = LinearLayoutManager(context)

        viewAdapter = TitlesListAdapter(articles, object: TitlesListAdapter.OnItemEventListener<Article> {
            override fun onClick(position: Int, article: Article) {
                listener?.onArticleSelected(article.id)
            }

            override fun onDragIconPressed(position: Int) {
                itemTouchHelper.startDrag(recyclerview_fragmentTitles_titlesList.findViewHolderForAdapterPosition(position)!!)
            }


            override fun isSwipeable(article: Article): Boolean {
                // Does not allow swiping out the same article which is currently selected
                if (listener != null) {
                    return !listener!!.isArticleSelected(article.id)
                }

                return true
            }

        })


        recyclerview_fragmentTitles_titlesList.apply recyclerView@{
            setHasFixedSize(true)
            layoutManager = viewLayoutManager
            adapter = viewAdapter

            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL)) // Add divider bars between items

            /** Handles swipe and drag events */
            itemTouchHelper = ItemTouchHelper(BasicItemTouchHelperCallback(viewAdapter, false, true)).apply {
                attachToRecyclerView(this@recyclerView)
            }

        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_titles, container, false)
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnArticleSelectedListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnArticleSelectedListener")
        }
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnArticleSelectedListener {
        /**
         * Notify that an article has been selected
         * @param id ID of the article selected
         */
        fun onArticleSelected(id: Int)

        /**
         * Determine if an article is selected
         * @param id ID of the article
         * @return True if the article is actually selected
         */
        fun isArticleSelected(id: Int): Boolean
    }
}



/**
 * Adapter class for the RecyclerView inside the TitlesFragment.
 * This class is the bridge between the dataset (articles) and its representation inside the RecyclerView.
 * When the user clicks on a Title, the OnItemEventListener@onClick callback is invoked, passing the position inside
 * the adapter of the item that was clicked.
 * When the user presses the drag icon, the OnItemEventListener@onDragIconPressed is invoked, passing the position inside
 * the adapter of the item.
 */
class TitlesListAdapter(
    private val dataset: MutableList<Article>,
    val clickListener: OnItemEventListener<Article>) : RecyclerView.Adapter<TitlesListAdapter.TitlesViewHolder>(),
                                                       BasicItemTouchHelperCallback.ItemTouchHelperAdapter
{

    /**
     * Interface used to communicate click, drag and swipe events up in the chain
     */
    interface OnItemEventListener<T> {
        /**
         * Notify when an item in the list is clicked.
         * @param position The position, in the adapter, of the item that was clicked
         * @param element The element that was clicked
         */
        fun onClick(position: Int, element: T)


        /**
         * Notify when the user presses the drag icon.
         * @param position The position, in the adapter, of the item
         */
        fun onDragIconPressed(position: Int)


        /**
         * Ask if an element can be swiped by the user
         * @param element The element the user wants to swipe
         * @return True if the element can be swiped, false otherwise
         */
        fun isSwipeable(element: T): Boolean
    }


    override fun getItemCount() = dataset.size



    /**
     * Called when a ViewHolder must be bound to an item in the dataset. In this case we simply set the text field
     * of the TextView to the item's title
     */
    override fun onBindViewHolder(viewHolder: TitlesViewHolder, position: Int) {
        viewHolder.title.text = dataset[position].title
        viewHolder.itemView.isSelected = dataset[position].id == selectedArticleId
    }



    /**
     * Called when a new ViewHolder object is created. We need to return the layout associated to this ViewHolder.
     * In this case it is the listitem_fragment_titles layout.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TitlesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.listitem_fragment_titles, parent, false)
        return TitlesViewHolder(view)
    }



    /**
     * Custom ViewHolder class. Declared as "inner" so it can access the "clickListener" variable from the outer class
     */
    inner class TitlesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.textView_listitemFragmentTitles_title

        /**
         * We need to create an OnClickListener for the TextView and a OnTouchListener for the dragIcon.
         * The dragIcon listener must fire as soon as the user presses the icon, so we cannot use a normal "onClick"
         * event.
         */
        init {
            title.setOnClickListener {
                val oldSelection = selectedArticleId
                clickListener.onClick(adapterPosition, dataset[adapterPosition])

                if (oldSelection != -1) {
                    notifyItemChanged(dataset.indexOfFirst { it.id == oldSelection })
                }
                notifyItemChanged(adapterPosition)
            }

            view.imageview_all_dragicon.setOnTouchListener { _, event ->
                if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                    clickListener.onDragIconPressed(adapterPosition)
                    return@setOnTouchListener false
                }
                false
            }
        }
    }




    /************************************************************************************
     *
     *  Override methods from BasicItemTouchHelperCallback.ItemTouchHelperAdapter
     *
     ***********************************************************************************/

    override fun onItemDismiss(position: Int) {
        dataset.removeAt(position)
        notifyItemRemoved(position)
    }


    override fun onItemMove(from: Int, to: Int) {
        val tmp = dataset[to]
        dataset[to] = dataset[from]
        dataset[from] = tmp
        notifyItemMoved(from, to)
    }

    override fun isSwipeable(position: Int): Boolean {
        return clickListener.isSwipeable(dataset[position])
    }
}


