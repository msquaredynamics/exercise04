# Exercise04

This is the same app developed in the Exercise03 (step 3), but uses a RecyclerView to display the list of Titles, instead of hardcoded titles.

In this sample code other features have been included for future reference:

- Different background color for the selected title, in landscape mode
- Possibility to move (drag and drop) a title up and down inside the list
- Possibility to swipe a title to remove it from the list


